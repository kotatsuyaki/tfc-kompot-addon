package io.gitlab.akitakilab.KompotMod.Containers;

import com.bioxx.tfc.Containers.ContainerTFC;
import com.bioxx.tfc.Containers.Slots.SlotSizeSmall;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;


public class ContainerGlassContainer extends ContainerTFC {
	private World world;
	public InventoryCrafting inv;

	public ContainerGlassContainer(InventoryPlayer pinv, World w, int x, int y, int z) {
		player = pinv.player;
		world = w;
		inv = new InventoryCrafting(this, 2, 2);
		layoutContainer(pinv, 0, 0);
		bagsSlotNum = player.inventory.currentItem; // currentItem is actually an int

		if (!world.isRemote)
			loadBagInv();
	}

	private void layoutContainer(IInventory playerInventory, int xSize, int ySize) {
		addSlotToContainer(new SlotSizeSmall(inv, 0, 71, 25));
		addSlotToContainer(new SlotSizeSmall(inv, 1, 89, 25));
		addSlotToContainer(new SlotSizeSmall(inv, 2, 71, 43));
		addSlotToContainer(new SlotSizeSmall(inv, 3, 89, 43));

		for (int row = 0; row < 3; row++)
			for (int col = 0; col < 9; col++)
				this.addSlotToContainer(new Slot(playerInventory, col + row * 9+9, 8 + col * 18, 90 + row * 18));
	}

	private void loadBagInv() {
		ItemStack bagIs = player.inventory.getStackInSlot(bagsSlotNum);
		if (bagIs == null)
			return;
		if (!bagIs.hasTagCompound())
			return;

		NBTTagList tagList = bagIs.getTagCompound().getTagList("Items", 4);
		for(int i = 0; i < tagList.tagCount(); i++) {
			NBTTagCompound tagComp = tagList.getCompoundTagAt(i);
			byte slotByte = tagComp.getByte("Slot");

			ItemStack is = ItemStack.loadItemStackFromNBT(tagComp);
			if (is.stackSize == 0)
				this.inv.setInventorySlotContents(slotByte, null);
			else
				this.inv.setInventorySlotContents(slotByte, is);
		}
	}
	@Override
	public void saveContents(ItemStack is) {
		NBTTagList tagList = new NBTTagList();

		for (int i = 0; i <= 4; i++) {
			ItemStack contentStack = inv.getStackInSlot(i);
			if (contentStack != null) {
				NBTTagCompound tagComp = new NBTTagCompound();
				tagComp.setByte("Slot", (byte)i);
				contentStack.writeToNBT(tagComp);
				tagList.appendTag(tagComp);
			}
		}

		if(is != null) {
			if(!is.hasTagCompound())
				is.setTagCompound(new NBTTagCompound());
			is.getTagCompound().setTag("Items", tagList);
		}
	}

	@Override
	public ItemStack loadContents(int slot) {
		ItemStack bagIs = player.inventory.getStackInSlot(bagsSlotNum);

		if (bagIs == null)
			return null;
		if (!bagIs.hasTagCompound())
			return null;

		NBTTagList tagList = player.inventory.getStackInSlot(bagsSlotNum).getTagCompound().getTagList("Items", 4);

		if (tagList == null)
			return null;

		for (int i = 0; i <= 4; i++) {
			NBTTagCompound tagComp = tagList.getCompoundTagAt(i);
			byte slotByte = tagComp.getByte("Slot");
			if (slotByte == slot)
				return ItemStack.loadItemStackFromNBT(tagComp);
		}

		return null;
	}
}
