package io.gitlab.akitakilab.KompotMod.Items;

import com.bioxx.tfc.Items.ItemTerra;
import io.gitlab.akitakilab.KompotMod.KompotMod;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;


public class ItemKompot extends ItemTerra {
	@Override
	public void registerIcons(IIconRegister reg) {
		this.itemIcon = reg.registerIcon(KompotMod.MODID + ":" + this.textureFolder +
										 this.getUnlocalizedName().replace("item.", ""));
	}

	@Override
    public String getItemStackDisplayName(ItemStack itemstack){
		return StatCollector.translateToLocal(getUnlocalizedName() + ".name");
    }
}
