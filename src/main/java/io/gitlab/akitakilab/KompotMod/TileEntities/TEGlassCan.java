package io.gitlab.akitakilab.KompotMod.TileEntities;

import com.bioxx.tfc.api.TFCItems;
import java.util.List;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;


public class TEGlassCan extends TileEntity implements IInventory {
	public boolean isEmpty;
	public boolean sealed;
	public GlassCanContentType content;
	private ItemStack itemSlot;
	
	public TEGlassCan() {
		isEmpty = true;
		sealed = true;
	}

	@Override
	public void updateEntity() {
		// Early return if it's client side
		if (worldObj.isRemote)
			return;

		// Early return if the slot is not empty
		if (itemSlot != null)
			return;

		// Get the list of item entities above the can
		@SuppressWarnings("unchecked")
		List<EntityItem> list = worldObj.getEntitiesWithinAABB(EntityItem.class, AxisAlignedBB.getBoundingBox(xCoord, yCoord + 1, zCoord, xCoord + 1, yCoord + 1.5, zCoord + 1));

		// Guard the list
		if (list == null || list.isEmpty())
			return;

		for (EntityItem entity : list) {
			ItemStack is = entity.getEntityItem();
			Item item = is.getItem();

			// TODO: Change this with real recipe code
			if (item == TFCItems.redApple) {
				this.setInventorySlotContents(0, is);
				entity.setDead();
			}
		}
	}

	public void careForInventorySlot() {
		// TODO: Update content status
		if (itemSlot.getItem() == TFCItems.redApple) {
			this.content = GlassCanContentType.Kompot;
			this.itemSlot = null;
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound tagComp) {
		super.readFromNBT(tagComp);

		isEmpty = tagComp.getBoolean("isEmpty");
	}

	@Override
	public void writeToNBT(NBTTagCompound tagComp) {
		super.writeToNBT(tagComp);

		tagComp.setBoolean("isEmpty", isEmpty);
	}

	@Override
	public int getInventoryStackLimit() {
		return 1;
	}

	@Override
	public int getSizeInventory() {
		return 1;
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack is) {
		return true;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public boolean hasCustomInventoryName() {
		return true;
	}

	@Override
	public String getInventoryName() {
		return "Glass Can";
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack is) {
		if (slot == 0)
			itemSlot = is;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot) {
		return null;
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount) {
		ItemStack stack = itemSlot;
		if (stack != null) {
			if (stack.stackSize <= amount) {
				setInventorySlotContents(0, null);
			} else {
				stack = stack.splitStack(amount);
			}
		}
		return stack;
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return itemSlot;
	}

	@Override
	public void openInventory() {
	}

	@Override
	public void closeInventory() {
	}

	public enum GlassCanContentType {
		Kompot
	}
}
