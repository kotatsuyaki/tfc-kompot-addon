package io.gitlab.akitakilab.KompotMod.Gui;

import com.bioxx.tfc.GUI.GuiContainerTFC;
import net.minecraft.inventory.Container;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.world.World;
import io.gitlab.akitakilab.KompotMod.Containers.ContainerGlassContainer;
import net.minecraft.util.ResourceLocation;
import io.gitlab.akitakilab.KompotMod.KompotMod;


public class GuiGlassContainer extends GuiContainerTFC {
	public static ResourceLocation texture = new ResourceLocation(KompotMod.MODID, "textures/gui/GuiGlassContainer.png");

	public GuiGlassContainer(InventoryPlayer inventoryplayer, World world, int i, int j, int k) {
		super(new ContainerGlassContainer(inventoryplayer, world, i, j, k), 176, 85);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
		this.drawGui(texture);
	}
}
