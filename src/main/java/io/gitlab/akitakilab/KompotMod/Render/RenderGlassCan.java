package io.gitlab.akitakilab.KompotMod.Render;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import io.gitlab.akitakilab.KompotMod.KompotMod;
import io.gitlab.akitakilab.KompotMod.ModManager;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEGlassCan;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import org.lwjgl.opengl.GL11;


public class RenderGlassCan implements ISimpleBlockRenderingHandler {
	@Override
	public int getRenderId() {
		return ModManager.glassCanBlockRenderID;
	}

	@Override
	public boolean shouldRender3DInInventory(int modelId) {
		return true;
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int i, int j, int k,
									Block block, int modelId,
									RenderBlocks renderer) {
		TileEntity entity = world.getTileEntity(i, j, k);
		if (!(entity instanceof TEGlassCan))
			return false;

		TEGlassCan te = (TEGlassCan) entity;

		if (KompotMod.proxy.currentRenderPass == 0 && te.sealed) {
			// Opaque render pass
			// Only do this if it's sealed
			renderer.setRenderBounds(0.3F, 0.91F, 0.3F, 0.7F, 1F, 0.7F);
			renderer.renderStandardBlock(ModManager.bronzeBlock, i, j, k);
		} else {
			// Transparent render pass
			// Render four sides
			renderer.setRenderBounds(0.3F, 0.2F, 0.3F, 0.65F, 0.9F, 0.35F);
			renderer.renderStandardBlock(Blocks.stained_glass, i, j, k);

			renderer.setRenderBounds(0.65F, 0.2F, 0.3F, 0.7F, 0.9F, 0.65F);
			renderer.renderStandardBlock(Blocks.stained_glass, i, j, k);

			renderer.setRenderBounds(0.35F, 0.2F, 0.65F, 0.7F, 0.9F, 0.7F);
			renderer.renderStandardBlock(Blocks.stained_glass, i, j, k);

			renderer.setRenderBounds(0.3F, 0.2F, 0.35F, 0.35F, 0.9F, 0.7F);
			renderer.renderStandardBlock(Blocks.stained_glass, i, j, k);

			// Render body block
			renderer.setRenderBounds(0.2F, 0F, 0.2F, 0.8F, 0.8F, 0.8F);
			renderer.renderStandardBlock(Blocks.stained_glass, i, j, k);
		}
		
		return true;
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		// Render the lid
		renderer.setRenderBounds(0.3F, 0.91F, 0.3F, 0.7F, 1F, 0.7F);
		renderInvBlock(ModManager.bronzeBlock, 0, renderer);

		// Render four sides
		renderer.setRenderBounds(0.3F, 0.2F, 0.3F, 0.65F, 0.9F, 0.35F);
		renderInvBlock(Blocks.stained_glass, metadata, renderer);

		renderer.setRenderBounds(0.65F, 0.2F, 0.3F, 0.7F, 0.9F, 0.65F);
		renderInvBlock(Blocks.stained_glass, metadata, renderer);

		renderer.setRenderBounds(0.35F, 0.2F, 0.65F, 0.7F, 0.9F, 0.7F);
		renderInvBlock(Blocks.stained_glass, metadata, renderer);

		renderer.setRenderBounds(0.3F, 0.2F, 0.35F, 0.35F, 0.9F, 0.7F);
		renderInvBlock(Blocks.stained_glass, metadata, renderer);

		// Render body block
		renderer.setRenderBounds(0.2F, 0F, 0.2F, 0.8F, 0.8F, 0.8F);
		renderInvBlock(Blocks.stained_glass, metadata, renderer);
	}

	public static void renderInvBlock(Block block, int m, RenderBlocks renderer) {
		Tessellator tes = Tessellator.instance;
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		tes.startDrawingQuads();
		tes.setNormal(0.0F, -1.0F, 0.0F);
		renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(0.0F, 1.0F, 0.0F);
		renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(0.0F, 0.0F, -1.0F);
		renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(0.0F, 0.0F, 1.0F);
		renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(-1.0F, 0.0F, 0.0F);
		renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, m));
		tes.draw();
		tes.startDrawingQuads();
		tes.setNormal(1.0F, 0.0F, 0.0F);
		renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, m));
		tes.draw();
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
	}
}
