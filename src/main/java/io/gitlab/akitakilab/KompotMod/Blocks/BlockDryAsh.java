package io.gitlab.akitakilab.KompotMod.Blocks;

import com.bioxx.tfc.Blocks.BlockTerra;
import net.minecraft.block.material.Material;
import com.bioxx.tfc.Core.TFCTabs;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import io.gitlab.akitakilab.KompotMod.KompotMod;
import cpw.mods.fml.relauncher.SideOnly;
import cpw.mods.fml.relauncher.Side;


public class BlockDryAsh extends BlockTerra {
	private IIcon icon;
	public BlockDryAsh() {
		super(Material.sand);
		this.setCreativeTab(TFCTabs.TFC_MISC);
		this.setBlockBounds(0f, 0, 0f, 1f, 1, 1f);
		this.lightOpacity = 255;
	}

	@Override
	public void registerBlockIcons(IIconRegister reg) {
		icon = reg.registerIcon(KompotMod.MODID + ":" + "DryAshOnRack");
	}

	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int face, int metadata) {
		return icon;
	}
}
