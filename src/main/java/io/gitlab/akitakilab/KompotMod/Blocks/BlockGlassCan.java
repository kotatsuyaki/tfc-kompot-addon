package io.gitlab.akitakilab.KompotMod.Blocks;

import net.minecraft.block.Block;
import com.bioxx.tfc.Blocks.BlockTerraContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEGlassCan;
import io.gitlab.akitakilab.KompotMod.ModManager;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import cpw.mods.fml.relauncher.SideOnly;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.init.Blocks;
import net.minecraftforge.common.util.ForgeDirection;
import io.gitlab.akitakilab.KompotMod.KompotMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.block.material.MaterialTransparent;
import com.bioxx.tfc.api.TFCBlocks;

public class BlockGlassCan extends BlockTerraContainer {
	public BlockGlassCan() {
		super(Material.rock);
		this.setCreativeTab(CreativeTabs.tabDecorations);
		this.setBlockBounds(0.2f, 0, 0.2f, 0.8f, 1f, 0.8f);
	}

	@Override
	public TileEntity createNewTileEntity(World w, int meta) {
		return new TEGlassCan();
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public IIcon getIcon(int side, int meta) {
		return TFCBlocks.planks.getIcon(side, meta);
	}

	@Override
	public int getRenderType() {
		return ModManager.glassCanBlockRenderID;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderBlockPass() {
		return 2;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean canRenderInPass(int pass) {
		// Save the render pass
		KompotMod.proxy.currentRenderPass = pass;
		// Always render
		return true;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		if (world.isRemote) {
			world.markBlockForUpdate(x, y, z);
			return true;
		} else {
			if (player.isSneaking())
				return false;
			
			if (world.getTileEntity(x, y, z) instanceof TEGlassCan) {
			}
		}
		return false;
	}
}
