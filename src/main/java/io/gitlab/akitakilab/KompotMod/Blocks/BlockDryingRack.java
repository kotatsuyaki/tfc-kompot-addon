package io.gitlab.akitakilab.KompotMod.Blocks;

import com.bioxx.tfc.Blocks.BlockTerraContainer;
import com.bioxx.tfc.api.TFCBlocks;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.gitlab.akitakilab.KompotMod.ModManager;
import io.gitlab.akitakilab.KompotMod.TileEntities.TEDryingRack;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraftforge.common.util.ForgeDirection;


public class BlockDryingRack extends BlockTerraContainer {
	public IIcon wetAshIcon;

	public BlockDryingRack() {
		super(Material.wood);
		this.setCreativeTab(CreativeTabs.tabDecorations);
		this.setBlockBounds(0, 0, 0, 1f, 0.1f, 1f);
	}

	@Override
	public TileEntity createNewTileEntity(World w, int meta) {
		return new TEDryingRack();
	}

	@Override
	public boolean canPlaceBlockAt(World w, int x, int y, int z) {
		return w.isSideSolid(x, y - 1, z, ForgeDirection.UP);
	}

	@Override
	public void onNeighborBlockChange(World w, int x, int y, int z, Block block) {
		if (!w.isSideSolid(x, y - 1, z, ForgeDirection.UP)) {
			w.setBlockToAir(x, y, z);
			dropBlockAsItem(w, x, y, z, new ItemStack(ModManager.dryingRackBlock, 1, w.getBlockMetadata(x, y, z)));
		}
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public int getRenderType() {
		return ModManager.dryingRackRenderID;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderBlockPass() {
		return 1;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean canRenderInPass(int pass) {
		return true;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ) {
		super.onBlockActivated(world, x, y, z, player, side, hitX, hitY, hitZ);

		if (world.isRemote) {
			return true;
		} else {
			TileEntity entity = world.getTileEntity(x, y, z);
			if (!(entity instanceof TEDryingRack))
				return true;
			TEDryingRack te = (TEDryingRack) entity;
			handleInteraction(player, te);
			return true;
		}
	}

	/**
	 * Must override this, or it would always drop with metadata 0
	 * regardless of wood type.
	 */
	@Override
	public int damageDropped(int meta) {
		return meta;
	}

	/**
	 * Accept or drop the item in inventory
	 */
	private void handleInteraction(EntityPlayer p, TEDryingRack te) {
		ItemStack is = p.getCurrentEquippedItem();

		if (is == null) {
			// Empty hand
			if (te.itemSlot != null)
				te.dropItemIfNotEmpty();
		} else {
			// Holding something
			if (te.acceptsItem(is)) {
				ItemStack afterIs = te.add(is);
				p.inventory.setInventorySlotContents(p.inventory.currentItem, afterIs);
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int face, int metadata) {
		return TFCBlocks.planks.getIcon(face, metadata);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess par1iBlockAccess, int par2, int par3, int par4, int par5) {
		return true;
	}
}
